<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${message.text}の編集</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessages">
						<li><c:out value="${errorMessages}" />
					</c:forEach>
				</ul>
			</div>

		</c:if>

		<form action="edit" method="post">
			<label for="text"></label>

			<textarea name="text" cols="100" rows="5" id="text">
					<c:out value="${message.text}" />
				</textarea>

			<input name="id" value="${message.id}" id="id" type="hidden" /> <input
				type="submit" value="更新" />140字まで <br />
		</form>
		<a href="./">戻る</a>

		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>